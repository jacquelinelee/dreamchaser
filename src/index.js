import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {jsonConfig, getJson} from './api/parseJSON'
import TestButtons from './Components/RenderTestButtons'
import AdaptiveCard from './Components/AdaptivePreview'
import { findAllByDisplayValue } from '@testing-library/react';

var createReactClass = require('create-react-class');

function PrintElement({element, removeElement, swapElement, updateState, displayImage, updateSubState}) {
    var content = getContent(element, updateState, displayImage, updateSubState)
    return (
        <React.Fragment>
        <div className="inputContainer">
            <div className="idContainer">{element.id}.
                <select id={"type".concat(element.id)} onChange={() => updateState(element.index, "type")} value={element.type}>
                  <option value="Text">Text</option>
                  <option value="Image">Image</option>
                  <option value="Form">Form</option>
                  <option value="Calendar">Calendar</option>
                  <option value="Checkbox">Checkbox</option>
                  <option value="Toggle">Toggle</option>
                  <option value="FactSet">Fact Set</option>
                  <option value="Button">Button</option>
                </select>
                <button className="upButton" id={"upButton".concat(element.id)} onClick={() => swapElement(parseInt(element.index,10), parseInt(element.index,10)-1)}>▲</button>
                <button className="downButton" id={"downButton".concat(element.id)} onClick={() => swapElement(parseInt(element.index,10), parseInt(element.index,10)+1)}>▼</button>
            </div>
            <div className="elementContainer" id={element.id}>
                <div className="elementContent">
                    {content}
                </div>
                <div className="elementDeleteButtonContainer">
                    <button className="elementDeleteButton" onClick = {() => removeElement(element.index)}>Delete</button>
                </div>
            </div>
        </div>
        </React.Fragment>
    );
}

function getContent(element, updateState, displayImage, updateSubState) {
    var type = element.type;
    var id = element.id;
    var index = element.index;
    if (type === "Text") {
        return (
            <React.Fragment>
            Title?
            <input id={"title".concat(id)} type="checkbox" onChange={() => updateState(index, "title")} checked={element.title}/>
            <input id={"textInput".concat(id)} type="text" onChange={() => updateState(index, "textInput")} className="elementTextInput" placeholder="Insert text here..."  value={element.textInput} />
            </React.Fragment>
        );
    }
    else if (type === "Image") {
        return (
            <React.Fragment>
            URL:
            <input id={"imgURL".concat(id)} type="text" className="elementTextInput" placeholder="Insert URL here..." onChange={() => displayImage(id)} value={element.imageURL} />
            <img id={"imgDisplay".concat(id)} className="elementImgDisplay" src={element.imgDisplayURL} />
            </React.Fragment>
        );
    }
    else if (type === "Button"){
        return (
            <React.Fragment>
            Name:
            <input id={"buttonName".concat(id)} type="text" className="elementTextInput" onChange={() => updateState(index, "buttonName")} placeholder="Insert name here..." value={element.buttonName} />
            URL:
            <input id={"buttonURL".concat(id)} type="text" className="elementTextInput" placeholder="Insert URL here..." value={element.buttonURL} onChange={() => updateState(index, "buttonURL")} />
            </React.Fragment>
        );
    }
    else if (type === "Form"){
        var formFields = getFormFields(element, updateSubState);
        return (
            <React.Fragment>
            Name:
            <input id={"formName".concat(id)} type="text" className="elementTextInput" onChange={() => updateState(index, "formName")} placeholder="Insert name here..." value={element.formName} />
            Number of fields:
            <select id={"formNumber".concat(element.id)} onChange={() => updateState(element.index, "formNumber")} value={element.formNumber}>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </select>
            <div id={"formContainer".concat(id)} className="formContainer">
                {formFields}
            </div>
            Submit button text:
            <input id={"formSubmitText".concat(id)} type="text" className="elementTextInput" onChange={() => updateState(index, "formSubmitText")} placeholder="Submit!" value={element.formSubmitText} />
            </React.Fragment>
        );
    }
    else if (type === "Calendar"){
        return (
            <React.Fragment>
            Text:
            <input id={"calendarText".concat(id)} type="text" className="elementTextInput" onChange={() => updateState(index, "calendarText")} placeholder="Insert text here..." value={element.calendarText} />
            Button Text:
            <input id={"calendarButtonText".concat(id)} type="text" className="elementTextInput" onChange={() => updateState(index, "calendarButtonText")} placeholder="Insert button text here..." value={element.calendarButtonText} />
            </React.Fragment>
        );
    }
    else if (type === "Checkbox"){
        var checkboxFields = getCheckboxFields(element, updateSubState);
        return (
            <React.Fragment>
            Question:
            <input id={"checkboxQuestion".concat(id)} type="text" className="elementTextInput" onChange={() => updateState(index, "checkboxQuestion")} placeholder="What is your question?" value={element.checkboxQuestion} />
            Number of checkboxes:
            <select id={"checkboxNumber".concat(element.id)} onChange={() => updateState(element.index, "checkboxNumber")} value={element.checkboxNumber}>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select>
            <div id={"checkboxContainer".concat(id)} className="checkboxContainer">
                {checkboxFields}
            </div>
            Submit button text:
            <input id={"checkboxSubmitText".concat(id)} type="text" className="elementTextInput" onChange={() => updateState(index, "checkboxSubmitText")} placeholder="Submit!" value={element.checkboxSubmitText} />
            </React.Fragment>
        );
    }
    else if (type === "Toggle"){
        var toggleFields = getToggleFields(element, updateSubState);
        return (
            <React.Fragment>
            Section title:
            <input id={"toggleQuestion".concat(id)} type="text" className="elementTextInput" onChange={() => updateState(index, "toggleQuestion")} placeholder="Insert title here..." value={element.toggleQuestion} />
            Number of toggles:
            <select id={"toggleNumber".concat(element.id)} onChange={() => updateState(element.index, "toggleNumber")} value={element.toggleNumber}>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select>
            <div id={"toggleContainer".concat(id)} className="checkboxContainer">
                {toggleFields}
            </div>
            Submit button text:
            <input id={"toggleSubmitText".concat(id)} type="text" className="elementTextInput" onChange={() => updateState(index, "toggleSubmitText")} placeholder="Submit!" value={element.toggleSubmitText} />
            </React.Fragment>
        );
    }
    else if (type === "FactSet"){
        var factSets = getFactSet(element, updateSubState);
        return (
            <React.Fragment>
            Number of facts:
            <select id={"factNumber".concat(element.id)} onChange={() => updateState(element.index, "factNumber")} value={element.factNumber}>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
            <div id={"factContainer".concat(id)} className="factContainer">
                {factSets}
            </div>
            </React.Fragment>
        );
    }
}

function getFormFields(element, updateSubState) {
    var fields = [];
    for (var i = 0; i < parseInt(element.formNumber,10); i++) {
        var fieldIdStart = Object.assign("formPrompt".concat(i));
        fields.push("Field:");
        fields.push(<input id={fieldIdStart.concat(element.id)} type="text" className="elementTextInput" placeholder="Insert prompt here..." value={element[fieldIdStart.concat(element.id)]} onChange={() => updateSubState(element.index, "form")} />);
    };
    return (fields);
}

function getCheckboxFields(element, updateSubState) {
    var fields = [];
    for (var i = 0; i < parseInt(element.checkboxNumber,10); i++) {
        fields.push("Checkbox Option:")
        fields.push(<input id={"checkboxOption".concat(i, element.id)} type="text" className="elementTextInput" placeholder="Insert option here..." value={element["checkboxOption".concat(i, element.id)]} onChange={() => updateSubState(element.index, "checkbox")} />)
    };
    return (fields);
}

function getToggleFields(element, updateSubState) {
    var fields = [];
    for (var i = 0; i < parseInt(element.toggleNumber,10); i++) {
        fields.push("Toggle Option:")
        fields.push(<input id={"toggleOption".concat(i, element.id)} type="text" className="elementTextInput" placeholder="Insert option here..." value={element["toggleOption".concat(i, element.id)]} onChange={() => updateSubState(element.index, "toggle")} />)
    };
    return (fields);
}

function getFactSet(element, updateSubState) {
    var fields = [];
    for (var i = 0; i < parseInt(element.factNumber,10); i++) {
        fields.push("Field:")
        fields.push(<input id={"field".concat(i, element.id)} type="text" className="elementTextInput" placeholder="Insert text here..." value={element["field".concat(i, element.id)]} onChange={() => updateSubState(element.index, "field")} />)
        fields.push("Fact:")
        fields.push(<input id={"fact".concat(i, element.id)} type="text" className="elementTextInput" placeholder="Insert text here..." value={element["fact".concat(i, element.id)]} onChange={() => updateSubState(element.index, "fact")} />)
    };
    return (fields);
}

var App = createReactClass({
    // This list is the core of the functionality. It's where all elements are stored.
    elementList : [{ index: 0, id: 1, type: "Text" }],
    // This function adds a new element to the elementList
    addElement : function() {
        // Creating default element
        const newElement = {
            index: this.elementList.length,
            id: (this.elementList.length + 1).toString(),
            creationDate: new Date().getTime(),
            type: "Text",
            formNumber: "1",
            checkboxNumber: "2"
        };
        // Pushing to list
        this.elementList.push(newElement);
        // If theres 8 elements, hide the add element button
        if (this.elementList.length === 8) {
            document.getElementById("addElementButton").style.display = "none";
        }
        // Resetting state to reflect changes
        this.setState({ elementList : this.elementList });
    },
    // This function removes an element from the elementList by index
    removeElement : function(removeIndex) {
        // Removing element and then iterating over remaining
        // elements in order to ensure correct indicies & id's
        this.elementList.splice(removeIndex, 1);
        for(var i=0; i < this.elementList.length; i++) {
            this.elementList[i].id = (i+1).toString();
            this.elementList[i].index = i;
        };
        // Reset the add element button style in case it's been hidden
        document.getElementById("addElementButton").style.display = "inline-block";
        // Resetting state to reflect deletion
        this.setState({ elementList : this.elementList });
    },
    // This function swaps two elements at an index
    swapElement : function(elementIndex1, elementIndex2) {
        // Only run if inputs are valid
        if(elementIndex1 >= 0 && elementIndex1 < this.elementList.length && elementIndex2 >= 0 && elementIndex2 < this.elementList.length) {
            var store = Object.assign({}, this.elementList[elementIndex1]);
            this.elementList[elementIndex1] = this.elementList[elementIndex2];
            this.elementList[elementIndex2] = store;
            // Rewriting id's and indexes
            for(var i=0; i < this.elementList.length; i++) {
                this.elementList[i].id = (i+1).toString();
                this.elementList[i].index = i;
            };
            // Resetting state to reflect move
            this.setState({ elementList : this.elementList });
        }
    },
    // This function gets the value of the indexed element in the webpage
    // and writes it to the elementList
    updateState : function(index, idStart) {
        var value;
        var DOMElement = document.getElementById(idStart.concat(this.elementList[index].id));
        if(DOMElement.type === "checkbox") {
            value = DOMElement.checked;
        }
        else {
            value = DOMElement.value;
        }
        this.elementList[index][idStart] = value;
        this.setState({ elementList : this.elementList });
    },
    // This function gets the values a form or checkbox list and
    // loads them into an array
    updateSubState : function(index, mode) {
        var element = this.elementList[index];
        var i;
        if(mode === "checkbox") {
            for(i = 0; i < element.checkboxNumber; i++) {
                element["checkboxOption".concat(i)] = document.getElementById("checkboxOption".concat(i, element.id)).value
            }
        }
        else if(mode === "form") {
            for(i = 0; i < element.formNumber; i++) {
                element["formPrompt".concat(i)] = document.getElementById("formPrompt".concat(i, element.id)).value
            }
        }
        else if(mode === "toggle") {
            for(i = 0; i < element.toggleNumber; i++) {
                element["toggleOption".concat(i)] = document.getElementById("toggleOption".concat(i, element.id)).value
            }
        }
        else if(mode === "fact" || mode ==="field") {
            for(i = 0; i < element.factNumber; i++) {
                element[mode.concat(i)] = document.getElementById(mode.concat(i, element.id)).value
            }
        }
        this.setState({ elementList : this.elementList });
    },
    // Grabs an image url and writes it as a source for the element
    displayImage : function(id) {
        var element = this.elementList[(parseInt(id,10)-1)];
        var url = document.getElementById("imgURL".concat(element.id)).value;
        element.imgDisplayURL = url;
        element.imgURL = url;
        this.setState({ elementList : this.elementList });
    },
    // Copies input to clipboard
    copyToClipboard : function(str) {
        const el = document.createElement('textarea');
        el.value = str;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    },
    // Standard render function. Contains the logic to iterate through elements
    render: function() {
        console.log(this.elementList);
        var els = document.getElementsByClassName("ac-adaptiveCard");
        for(var i=0; i < els.length; i++) {
            els[i].remove();
        }
        return (
            <React.Fragment key="App">
                <p className="sectionTitle">Builder</p>
                <p className="sectionTitle">Preview</p>
                <div id="appDiv" key="AppDiv">
                    {this.elementList.map(element => {
                        return <PrintElement element={element}  key={element.creationDate} removeElement={this.removeElement} swapElement={this.swapElement} updateState={this.updateState} displayImage={this.displayImage} updateSubState={this.updateSubState}/>;
                    })}
                </div>
                <div id="previewContainer">
                    <AdaptiveCard className="preview" element={this.elementList}/>
                </div>
                <div id="buttonContainer" key="buttonContainer">
                    <button className="btn btn-primary" id="addElementButton" key="addElementButton" onClick={() => this.addElement()}>Add Element</button>
                    <button className="btn btn-primary" id="sendJSON" key="sendJSON" onClick={() => jsonConfig(this.elementList)}>Send Card</button>
                    <button className="btn btn-primary" id="copyJSON" key="sendJSON" onClick={() => this.copyToClipboard(getJson(this.elementList))}>Copy to Clipboard</button>
                </div>
            </React.Fragment>
        );
    }
});

ReactDOM.render(<App />, document.getElementById('app'));

export default App;
