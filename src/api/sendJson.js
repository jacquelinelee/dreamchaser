import fetch from 'node-fetch'
import {baseTemplate} from '../JSONExample/BaseTemplate'

// API Configuration details 
const apiToken = 'eyJhbGciOiJSUzUxMiIsInppcCI6IkRFRiJ9.eNqcWO9vmzAQ_VcmPndVuypa009zwUm9GMyMaStNk0UpmqI2IUtAm1T1f58hhOZHNz_nK3Dnu3d37x5-8Vb1g3flPZRllWezxWm9KpbeiZfVj9Ninhfm1e_iwTzIl0VWFY_e1fng8vzi7PNgODi7HJ54i2I5m65W03K-8q6-v3jzbNYYsVCTOObMJ4qJKNGSksB4WSxLY1BNC_Pxy-vrSf-9TyJNA6a0SFXCAqqJHkuRxv-zUaFWzJ9QpRUNY04UtR5jTEiqRNhGpWXKIRNfRCM21neSKeroHbHpkqD31E9bS7eDvorrBMmiOyagnKIh9bhiRpuvQ1N6FgGAmcZAsTWf0pAwjtf60MKexeQaj-iwxQMmqa8sNmlCpU4oH7n20wZVW95hYtzLW-Zj48AZsbdPgwuRpiuObWnbAeSbcSzkZMTFnT1DrgOiSCJS6W-qahLhFuRbQrGH34RiZsphpFqXOo1NUBRJkgsSOCbpm8-tqMc8HTOAPozrUBjYMK_7lOZCIGtkoGM6CzLWJAhZhEyRPZIG8Q4Vawc2p2r_hqhrodynEouF3sdCKmssfbNCWHfR0KZprdjtDFoSkTi5EcqNAOxBGTaMhGKjjhuPYXerSRMxUq6GpoMOTnzXY-VsEHfYLibLkMox1YogJ7A-GjeukFRJRm8RStxZYFACMD7Ge0xlyJIE4f_9jnFa1QA8a75Dken3IiICtrJEmHerUOuNYQtmizeg3fXOdEM0EhL_hkVUc0pkhAiN_Zrh0-UgYeyQmqEiERnTkEYKKhgq8LaWh4Pm6WoLWZBk4hR8u_WwFbUbDa5lgNXU0ykspY77fUA0IxcO448pxX74oahZFNB7NyVyxG6BBQOi-N84H_Hb9txaHDn8sLRihJd59vxB1vPHIn-CaQuD543lKMSg_fdIEo3OTSApuiPpoaXYT4-jXkDiTiKTYFswrBua6CUNhaIOv2qt_3UrJw7NkNVVOcuqaTn_-Ksu6gJVm0BHbwgDKdgOpggt7nE0MDFbggDr5YbEXMQXOrVHlR_WGG3Lu3UlgPeaTxG3B3oFWrn7SwYiefAX8p2LM-dbJETvbLSRw4JyuxiFL_DeMk5j6FbjYLPhlxWYWt70_j_8_jjxfi7LetFcVntEdZvNM4-raf5UVOPNS_Ok-LNYX3cPzz9dDAfmwSybPu9fln_J8ryYV_WyOM3Lmff6FwAA__8.MP79DyYIXHEC_okfNV79FKubwICAvyRqZ7Hm7tAcz5hgUb3AftFp5JdZbGpVhOEx46q9adICgHKatLT-zF2_Q8VGv4PG13NkJv9y4fRkDH70jB34K19PRgSHCiKinIJ59zz6OChubNJWq32wi2fziC79AC5IRJ-4Kpnqgqa4d3o'
const apiURL = 'https://a04850dirautobot.aiam-dh.com/atr-gateway/teams-relay/api/v1/push/'

const sendJson = (body, message) => {
    /*
    This file serves to be a direct connection to the API endpoint, which the parseJSON file will call once
    it parses the data from the UI. 
    
    @param: 
    - body: The body attribute to be appended to the original JSON format. See 
    https://github.com/gatewayapps/react-adaptivecards for example 
    - message: The introductory message the bot will send initially. This is optional
    */

    // Attaches the 
    let base = baseTemplate
    base.attachments = body;

    // Allows the message to be optional 
    if (message == null) {
        base.message = ""
    } else {
        base.message = message;
    }

    // API Configuration option to be passed into the fetch function
    var options = {
        method: "POST",
        body: JSON.stringify(base),
        headers: {
            Authorization: `token ${apiToken}`,
            "Content-Type": "application/json"
        },
    };
    
    fetch(apiURL, options)
        .then((response) => {})
        .catch((error) => {alert("An error has occurred")})
}

export default sendJson;