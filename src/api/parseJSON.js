import sendJson from './sendJson'

function textModule(JSONInstance, input, title) {
    /**
     * This function handles a text module, and edits the JSON body to add a text object into the adaptive card
     * @returns updated JSON to send to Teams
     */
    
    if (title) {
        // Push to JSON 
        JSONInstance['body'].push({
            "type": "TextBlock",
            "text": input,
            "size": "large",
            "weight": "bolder",
            "wrap": false,
        })    
    }
    else {
        // Push to JSON 
        JSONInstance['body'].push({
            "type": "TextBlock",
            "text": input,
            "wrap": true,
        })    
    }
}

function imageModule(JSONInstance, url) {
    /**
     * This function handles an image module, and edits the JSON body to add a text object into the adaptive card
     * @returns updated JSON to send to Teams
     */
    
    // Push to JSON 
    JSONInstance['body'].push({
        "type": "Image",
        "url": url,
        "horizontalAlignment": "center"
    });
}

function urlModule(JSONInstance, buttonName, url) {
    /**
     * This function handles a button which opens a url module, and edits the JSON body to add an action object into the adaptive card
     * @returns updated JSON to send to Teams
     */
        
    // Push to JSON 
    JSONInstance['actions'].push({
        "type": "Action.OpenUrl",
        "title": buttonName,
        "url": url
    });
}

function formModule(JSONInstance, element) {
    /**
     * This function creates a form style card module and allows users to input the number of fields
     * @returns A ShowCard with a submittable form inside
     */
    
    // Grab ShowCard button name
    var buttonName = element.formName;
    // Grab number of input boxes
    var inputs = [];
    for(var i = 0; i < parseInt(element.formNumber,10); i++) {
        inputs.push(element["formPrompt".concat(i)])
    }
    
    // Grab submit button name
    var submitButton = element.formSubmitText;
    
    // Create ShowCard to nest the form
    var showCardStarter = '{"type": "Action.ShowCard", "title": "'+buttonName+'", "card": {"type": "AdaptiveCard", "body": [], "actions": []}}';
    var showCard = JSON.parse(showCardStarter);
    for (i=0; i<inputs.length; i++) {
        showCard.card.body.push({
            "type": "Input.Text",
            "id": (i + 1).toString(),
            "placeholder": inputs[i]
        });
    }
    // Create submit button
    showCard.card.actions.push({
        "type": "Action.Submit",
        "title": submitButton
    });
    JSONInstance.actions.push(showCard);
}

function calendarModule(JSONInstance, dateText, buttonText) {
    /**
     * This function creates an enterable calendar widget for users to enter and submit a date.
     * @returns Updated JSON in Adaptive Card format
     */
    // Push text to JSON
    JSONInstance.body.push({
        "type": "TextBlock",
        "text": dateText
    });
    // Push calendar format to JSON
    JSONInstance.body.push({
        "type": "Input.Date",
        "id": "date",
        "placeholder": "Enter a date",
        "value": "dd-mm-yyyy"
    });
    // Push submit button to JSON to save date data
    JSONInstance.actions.push({
        "type": "Action.Submit",
        "title": buttonText
    })
}

function checkboxModule(JSONInstance, element) {
    /**
     * This function creates a checkbox module with a question, choices and a submit button
     * @returns updated JSON 
     */
    
     // Text to be displayed above checkboxes
    var question = element.checkboxQuestion;
    // Text to put alongside checkboxes
    var boxInputs = [];
    for (var i = 0; i < parseInt(element.checkboxNumber, 10); i++) {
        boxInputs.push(element["checkboxOption".concat(i)])
    }
    // Name for submit button
    var buttonName = element.checkboxSubmitText;
    // Push question to JSON
    JSONInstance['body'].push(
        {
            "type": "TextBlock",
            "text": question,
        }
        );
    
    // Push ChoiceSet setup to JSON
    JSONInstance['body'].push(
        {
        "type": "Input.ChoiceSet",
        "id": question,
        "style": "expanded",
        "isMultiSelect": false,
        "value": "1",
        "choices": []
    });
    // Push JSON to 'choices' array from user input  
    var index = JSONInstance['body'].length - 1;
    for (i=0; i<boxInputs.length; i++) {
        JSONInstance.body[index].choices.push({
            "title": boxInputs[i],
            "value": boxInputs[i],
        });
    }
    
    JSONInstance.actions.push({
        "type": "Action.Submit",
        "title": buttonName,
    })
}

function toggleModule(JSONInstance, element) {
    var title = element.toggleQuestion;
    var numBoxes = element.toggleNumber;
    var toggleText = [];
    var buttonText = element.toggleSubmitText;
    
    for (var i=0; i<numBoxes; i++) {
        toggleText.push(element["toggleOption".concat(i)]);
    }
    // Push title to JSON
    JSONInstance['body'].push(
        {
            "type": "TextBlock",
            "text": title,
        }
    );
    // Push each toggle options to JSON
    for (i=0; i<toggleText.length; i++) {
        JSONInstance['body'].push(
            {
                "type": "Input.Toggle",
                "id": toggleText[i],
                "title": toggleText[i],
                "value": "false",
                "valueOn": "true",
                "valueOff": "false"
              }
        )
    }
    // Push submit button to JSON
    JSONInstance['actions'].push(
        {
            "type": "Action.Submit",
            "title": buttonText
          }
    )
}

function factModule(JSONInstance, element) {
    var numFacts = element.factNumber;
    var facts = [];
    // Get facts
    for (var i=0; i<numFacts; i++) {
        facts.push({
            "title": element["field".concat(i)], 
            "value": element["fact".concat(i)]
        })
    }
    // Push FactSet to JSON
    JSONInstance['body'].push(
        {
            "type": "FactSet",
            "facts": facts
        }
    );
}

function getJSONFormat(elementList) {
    var starter = '{"body": [], "actions": []}';
    let jsonToSend = JSON.parse(starter)
    //  Cycle through the divs/moduleTypes to edit the JSON
    for (var i=0; i<elementList.length; i++) {
        var element = elementList[i]
        switch (element.type) {
            case "Text":
                textModule(jsonToSend, element.textInput, element.title);
                break;
            case "Image":
                imageModule(jsonToSend, element.imgURL);
                break;
            case "Button":
                urlModule(jsonToSend, element.buttonName, element.buttonURL);
                break;
            case "Form":
                formModule(jsonToSend, element);
                break;
            case "Checkbox":
                checkboxModule(jsonToSend, element);
                break;
            case "Calendar":
                calendarModule(jsonToSend, element.calendarText, element.calendarButtonText);
                break;
            case "Toggle":
                toggleModule(jsonToSend, element);
                break;
            case "FactSet":
                factModule(jsonToSend, element);
                break;
            default:
                break;
        }
    }
    return jsonToSend;
}

function jsonConfig(elementList) {
    let jsonToSend = getJSONFormat(elementList);
    sendJson(jsonToSend, "");
}

function getJson(elementList) {
    let jsonToSend = getJSONFormat(elementList);
    return JSON.stringify(jsonToSend);
}
export {getJSONFormat, jsonConfig, getJson}
