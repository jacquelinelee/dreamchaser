/*
Example of how parseJSON would send through the JSON file to sendJSON
*/

export const emergency2 = 
{ 
    "body":[ 
       { 
          "type":"TextBlock",
          "text":"Emergency Safety Notification",
          "size":"large",
          "weight":"bolder",
          "wrap":false
       },
       { 
          "type":"TextBlock",
          "text":"Are you safe?"
       },
       { 
          "type":"Input.ChoiceSet",
          "id":"Are you safe?",
          "style":"expanded",
          "isMultiSelect":false,
          "value":"1",
          "choices":[ 
             { 
                "title":"Yes I am safe",
                "value":"Yes I am safe"
             },
             { 
                "title":"No I am not safe",
                "value":"No I am not safe"
             }
          ]
       }
    ],
    "actions":[ 
       { 
          "type":"Action.Submit",
          "title":"OK"
       }
    ]
 }