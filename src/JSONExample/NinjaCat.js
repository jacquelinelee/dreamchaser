/*
Example of how parseJSON would send through the JSON file to sendJSON
*/

export const ninjaCat = {
    "body": [
        {
            "type": "TextBlock",
            "text": "Here is a ninja cat"
        },
        {
            "type": "Image",
            "url": "http://adaptivecards.io/content/cats/1.png"
        }
    ]
}



/*
Below is an example of what swagger would expect as a POST response from sendJSON
{
    "language": "en",
    "message": "",
    "to": "Jiayi.chen@ucstage.onmicrosoft.com",
    "attachments": {
        "body": [
            {
                "type": "TextBlock",
                "text": "Here is a ninja cat"
            },
            {
                "type": "Image",
                "url": "http://adaptivecards.io/content/cats/1.png"
            }
        ]
    }
}
*/