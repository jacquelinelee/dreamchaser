/*
Example of how parseJSON would send through the JSON file to sendJSON 
*/

export const emergency = { 
    "body":[ 
       { 
          "type":"TextBlock",
          "text":"EMERGENCY NOTIFICATION"
       },
       { 
          "type":"Image",
          "url":"https://cdn.shopify.com/s/files/1/0641/1521/products/pins-baby-skeleton.jpg?v=1540320818"
       },
       { 
          "type":"TextBlock",
          "text":"NOTICE TO ALL STAFF:"
       },
       { 
          "type":"TextBlock",
          "text":"An army of Skeleton Men have annexed Sydney."
       },
       { 
          "type":"TextBlock",
          "text":" We recommend all staff stay indoors."
       },
       { 
          "type":"TextBlock",
          "text":" If you are safe please  click 'Yes'. "
       },
       { 
          "type":"TextBlock",
          "text":"If you require assistance please click 'No'."
       }
    ],
    "actions":[ 
       { 
          "type":"Action.OpenUrl",
          "title":"Yes I am safe",
          "url":"https://base.imgix.net/files/base/ebm/ehstoday/image/2019/04/ehstoday_9455_safetyfirst.png?auto=format&fit=crop&h=432&w=768"
       },
       { 
          "type":"Action.OpenUrl",
          "title":"No I need assistance",
          "url":"https://www.nsw.gov.au/services/services-by-topic/emergency-services-and-safety/"
       }
    ],
    "$schema":"http://adaptivecards.io/schemas/adaptive-card.json"
 }