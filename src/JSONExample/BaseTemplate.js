/*
The base template for the adaptive card for sending it to the API (attachments need to have a body/actions attribute) 
*/

export const baseTemplate = {
    // "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
    // "type": "AdaptiveCard",
    "language": "en",
    "message": "",
    "to": "Jiayi.chen@ucstage.onmicrosoft.com",
    "attachments": null
}