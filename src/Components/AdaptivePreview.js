import React, { Component } from "react"
import AdaptiveCard from 'react-adaptivecards'
import {getJSONFormat} from '../api/parseJSON' 
import {cloneDeep} from 'lodash'

// Mandatory attributes for the 'react-adaptivecard' library tag
const base = {
    'type': 'AdaptiveCard',
    'version': '1.0',
}

// Container styling
const containerStyle = {
    paddingTop: "25px",
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}

// The Adaptive Card styling
const cardStyle = {
    width: '500px'
}

export default class AdaptiveCardPreview extends Component {
    /*
    This class serves to render the preview of the adaptive card before sending it through
    
    @props
    - element: The list of attributes of the adaptive cards user has inputted
    */

    render() {

        // Clone the list of attributes user has entered in UI 
        var { element } = this.props  
        var thisElement = cloneDeep(element)
        
        // Get the JSON body format 
        let body = getJSONFormat(thisElement)

        // Append the customized body and action attributes to the base JSON variable
        base.body = body.body
        base.actions = body.actions

        return( 
            <div style={containerStyle}>
                <AdaptiveCard style={cardStyle} payload={base}/>
            </div>
        )
    }
}