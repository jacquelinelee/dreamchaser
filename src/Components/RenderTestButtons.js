import React, { Component } from "react"
import sendJson from '../api/sendJson'
import {jsonConfig} from '../api/parseJSON'
import { ninjaCat } from '../JSONExample/NinjaCat'
import { flightItinerary } from '../JSONExample/FlightItinerary'
import { emergency } from '../JSONExample/Emergency'
import { emergency2 } from '../JSONExample/Emergency2'

class TestButtons extends Component {
    
    /*
    This class only serves to unit tests modules. 

    Usage:
    - Import the file and module(s) you would like to test as shown in the first few lines 
    - Add a button as shown below 
    - Import this into index.js file (should be commented out but import it if not there)
    */

    render() {
        return( 
            <div>
                <button onClick={() => sendJson(flightItinerary, "Flight")}>Submit Flight Itinerary </button>
                <button onClick={() => sendJson(ninjaCat, "Ninja")}>Submit Ninja Cat </button>
                <button onClick={() => sendJson(emergency, "Emergency!!")}>Submit Emergency Notifications!</button>
                <button onClick={() => sendJson(emergency2, "Emergency!!")}>Submit Emergency2 Notifications!</button>
                <button onClick={() => jsonConfig()}>JSONConfig</button>
            </div>
        )
    }
}

export default TestButtons;