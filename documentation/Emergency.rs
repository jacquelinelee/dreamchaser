+ invasion underway
- <set attachments={    "body":[ { "type":"TextBlock", "text":"YOU NEED ASSISTANCE" }, { "type":"Image", "url":"http://www.quickmeme.com/img/9b/9b13ec090b1293df987e932a338566b73cb8474eb0944a23d711530d5a8637b5.jpg" }, { "type":"TextBlock", "text":"What kind of assistance do you need?" } ], "actions":[ { "type":"Action.OpenUrl", "title":"Ambulance", "url":"https://www.ambulance.nsw.gov.au/about-us/contact-us" }, { "type":"Action.OpenUrl", "title":"Fire Sevice", "url":"https://www.fire.nsw.gov.au/page.php?id=51" }, { "type": "Action.OpenUrl", "title": "Police Response", "url": "https://www.police.nsw.gov.au/contact_us" }, { "type": "Action.OpenUrl", "title": "Other", "url": "https://www.ses.nsw.gov.au/about-us/contact-us/" } ]}>

+ dumpling zoo
- <set attachments={    "body":[ { "type":"TextBlock", "text":"YOU ARE SAFE" }, { "type":"Image", "url":"https://sd.keepcalms.com/i-w600/keep-calm-you-are-safe.jpg" } ]}>

+ plastic button
- <set attachments={    "body":[ { "type":"TextBlock", "text":"HELP IS ON THE WAY" }, { "type":"Image", "url":"https://sd.keepcalms.com/i-w600/keep-calm-and-hold-on-help-is-on-the-way-.jpg" } ]}>
